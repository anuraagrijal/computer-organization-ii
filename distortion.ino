int in_ADC0
int in_ADC1;  //variables for 2 ADCs values (ADC0, ADC1)
const int POT0, POT1, POT2, out_DAC0, out_DAC1; //variables for 3 pots (ADC8, ADC9, ADC10)
const int LED = 3;
const int FOOTSWITCH = 7; 
const int TOGGLE = 2; 
const int DAC0 = 11;
const int DAC1 = 12;
const int ADC0 = 23;
const int ADC1 = 24

int upper_threshold, lower_threshold;


void setup()
{
  //DAC Configuration
  analogWrite(DAC0,0);  // Enables DAC0
  analogWrite(DAC1,0);  // Enables DAC1

  pinMode(39, OUTPUT);
}

void loop()
{
    in_ADC0 = analogRead(ADC0);               // read data from ADC0
    in_ADC1 = analogRead(ADC1);               // read data from ADC1  
    POT0 = analogRead(25);                    // read data from potentiometer 1        
    POT1 = analogRead(26);                    // read data from potentiometer 2  - not used
    POT2 = analogRead(27);                    // read data from potentiometer 3   - not used
    
    digitalWrite(39, HIGH);   // turn the LED on by making the LED pin HIGH
    delay(1000);               // wait for a second

    upper_threshold=map(POT0,0,4095,4095,2047);
    lower_threshold=map(POT0,0,4095,0000,2047);
 
    if(in_ADC0>=upper_threshold) {
      in_ADC0=upper_threshold;
    }
    else if(in_ADC0<lower_threshold){
      in_ADC0=lower_threshold;
    }
    
    if(in_ADC1>=upper_threshold) {
      in_ADC1=upper_threshold;
    }
    else if(in_ADC1<lower_threshold){
      in_ADC1=lower_threshold;
    }
    
    //adjust the volume with POT2
    out_DAC0=map(in_ADC0,0,4095,1,POT2);
    out_DAC1=map(in_ADC1,0,4095,1,POT2);
 
    analogWrite(DAC0, out_DAC0);
    analogWrite(DAC1, out_DAC1);
}